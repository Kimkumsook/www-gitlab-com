---
layout: handbook-page-toc
title: "Data Team Organization"
description: "GitLab Data Team Organization"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

---

## <i class="far fa-compass fa-fw icon-color font-awesome" aria-hidden="true"></i> Team Organization

The goal of our Data Fusion teams is to create **Business-Focused** and **Business-Involved** teams focused on GitLab’s most important business processes: lead-to-cash cycle and product-release-to-adoption as well as a Data Platform Enhancement team that focuses on iterating on the data platform supporting these business focused and involved teams.

Each Fusion Team supports a business division and works with an assigned busines-side "Data Champion" or stable counterpart to coordinate priorities and rquirements.

| **Go To Market (GTM)** | **Product** | **G&A** |
| ----- | ----- | ----- |
| @iweeks - Lead/DRI	| @ktam - Lead/DRI	| @pluthra - Lead/DRI | 
| @jpeguero |	@mpeychet_ | @ken_aguilar  |
| @jjstark | @m_walker | @vedprakash2021 |
| @paul_armstrong | @snalamaru | |
| |	@laddula | |	
						
The data fusion model emphasizes 2 elements:
1) Enagagement with Business Partners and Data Champions to build better products and drive towards the same goal
2) Visibility around what we do to get us to those goals

### Engagement with Business Partners and Data Champions includes:

1. Bi-weekly Issue/Status updates
1. Monthly Priority Alignment
1. Quarterly OKR review
1. Every 6 months a CSAT survey to provide feedback to the Data Fusion Team towards the goal of continuous improvement

We encourage our stakeholders to follow along with our issue boards to understand the scope of work:

1. [GTM](https://gitlab.com/gitlab-data/analytics/-/boards/1912663?&label_name[]=ft%3Al2c)
2. [Product](https://gitlab.com/groups/gitlab-data/-/boards/1912130?label_name[]=ft%3Ar2a)
3. [G&A](https://gitlab.com/groups/gitlab-data/-/boards/1435002?&label_name[]=People) 

<!-- need to update boards potentially based on new label names -->

### Visibility
Beyond the fore-mentioned engagement, we also have Monthly Release Communication and demo sessions to help our stakeholders understand what we have developed as a Data Team and how it can be utilized.

**Monthly Release Communications:**
<!-- (need to add issues with monthly release information or slide deck from OKRs -->


**Upcoming Demo Sessions:**


### Development Approach
In order to stay engaged and transparent on our work, we want our stakeholders to understand how the data team works and what they can expect out of us when we engage. We aim to follow the development process as seen below. This allows us to capture the requirements and goals of stakeholders upfront and include them in the process as we develop towards the end solution.
![data team development_process](data_team_development_process.png)


### Data Team Roles:

<div class="flex-row" markdown="0" style="height:80px">
  <a href="https://about.gitlab.com/job-families/finance/data-analyst/" class="btn btn-purple" style="width:33%;height:100%;margin:5px;float:left;display:flex;justify-content:center;align-items:center;">Data Analyst</a>
  <a href="https://about.gitlab.com/job-families/finance/data-engineer/" class="btn btn-purple" style="width:33%;height:100%;margin:5px;float:left;display:flex;justify-content:center;align-items:center;">Data Engineer</a>
  <a href="https://about.gitlab.com/job-families/finance/manager-data" class="btn btn-purple" style="width:33%;height:100%;margin:5px;float:left;display:flex;justify-content:center;align-items:center;">Manager</a>
  <a href="https://about.gitlab.com/job-families/finance/dir-data-and-analytics" class="btn btn-purple" style="width:33%;height:100%;margin:5px;float:left;display:flex;justify-content:center;align-items:center;">Director of Data & Analytics</a>
</div>



